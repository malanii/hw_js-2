"use strict";

let i = 0;
let myNumber = 0;

while (isNaN(myNumber) || myNumber == "" || myNumber < 0 || myNumber == null || myNumber % 1 === 1){
    myNumber = + prompt("enter number");
}

if (myNumber % 5 === 0) {
    for (i = 0; i <= myNumber; i += 5) {
        console.log(i)
    }
}
else {
    console.log("sorry, no numbers")
}
